import { CHANGE_CHART_TYPE, GET_CHART_TYPE } from "../constants/chart.constants"
import { chartInitialState } from "./initial-state"

export function chartReducer(state = chartInitialState, action) {
    switch (action.type) {
        case GET_CHART_TYPE:
            const chartType = localStorage.getItem('chartType')
            if (!chartType) return { chartInitialState }
            return {
                chartType
            };

        case CHANGE_CHART_TYPE:
            const newState = {
                ...state,
                chartType: action.chartType
            }
            localStorage.setItem('chartType', action.chartType)
            return newState;

        default:
            return state
    }
}