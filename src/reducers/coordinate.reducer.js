import { GET_ITEMS, ADD_ITEM, UPDATE_ITEM, REMOVE_ITEM } from '../constants/coordinate.constants'
import { coordinatesInitialState } from './initial-state'

export function coordinateReducer(state = coordinatesInitialState, action) {
    let newState = coordinatesInitialState;

    switch (action.type) {
        case GET_ITEMS:
            const storage = JSON.parse(localStorage.getItem('coordinates'));
            if (!storage) return { ...state }
            return {
                coordinates: [...storage.coordinates]
            }

        case ADD_ITEM:
            newState = {
                coordinates: [
                    ...state.coordinates,
                    { ...action.data }
                ]
            };
            localStorage.setItem('coordinates', JSON.stringify(newState));
            return newState;

        case UPDATE_ITEM:
            const { newData, oldData } = action.data
            state.coordinates[state.coordinates.indexOf(oldData)] = newData;
            newState = {
                ...state,
                hasUpdate: true
            };
            localStorage.setItem('coordinates', JSON.stringify(newState));
            return newState;

        case REMOVE_ITEM:
            const { data } = action
            state.coordinates.splice(state.coordinates.indexOf(data), 1);
            newState = {
                ...state,
                hasUpdate: true
            };
            localStorage.setItem('coordinates', JSON.stringify(newState));
            return newState;
            
        default:
            return state
    }
}