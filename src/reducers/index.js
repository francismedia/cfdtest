import { combineReducers } from 'redux';
import { chartReducer } from './chart.reducer';
import { coordinateReducer } from './coordinate.reducer';

const reducerMap = {
  main: coordinateReducer,
  chart: chartReducer
};

export default combineReducers(reducerMap);