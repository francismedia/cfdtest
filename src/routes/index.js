import React from 'react';
import Coordinates from '../containers/coordinates';
import Chart from '../containers/chart';
const routes = [
  {
    path: "/coordinates",
    component: Coordinates
  },
  {
    path: "/chart",
    component: Chart
  },
];

export default routes;