import React from 'react'
import {
  BrowserRouter as Router,
  Switch
} from "react-router-dom";
import { Provider } from 'react-redux';
import RouterView from './components/router-view';
import Header from './components/header';
import routes from './routes';
import store from './store';

function App() {
  return (
    <Provider store={store()}>
      <Router>
        <Header />
        <Switch>
          <React.Fragment>
            <div className="container content">
              {routes.map((route, i) => (
                <RouterView key={i} {...route} />
              ))}
            </div>
          </React.Fragment>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
