import React, { useEffect } from 'react'
import { Chart as GoogleChart } from "react-google-charts"
import { connect } from 'react-redux'
import coordinatesActions from '../actions/coordinates.actions';
import ChangeChart from './change-chart';
import chartActions from '../actions/chart.actions';

const Chart = ({
    fetch,
    coordinates,
    chartType,
}) => {
    useEffect(() => {
        fetch()
    }, []);
    const options = {
        title: "X axis vs. Y axis comparison",
        hAxis: { title: "X axis", viewWindow: { min: 0, max: 500 } },
        vAxis: { title: "Y axis", viewWindow: { min: 0, max: 500 } },
        legend: "none"
    };

    let data = coordinates.map(
        item => [parseInt(item.x, 10), parseInt(item.y, 10)]
    )
    data = [
        ['X', 'Y'],
        ...data
    ]

    return (<div>
        <ChangeChart></ChangeChart>
        <GoogleChart
            chartType={chartType}
            data={data}
            options={options}
            width="100%"
            height="400px"
            legendToggle
        />
    </div>)
};
const mapToState = state => {
    return {
        coordinates: state.main.coordinates,
        chartType: state.chart.chartType
    }
}
const mapToDispatch = dispatch => {
    return {
        fetch: () => dispatch(coordinatesActions.getCoordinates()),
    }
}

export default connect(mapToState, mapToDispatch)(Chart);
