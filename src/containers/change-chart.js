import React, { useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import chartActions from '../actions/chart.actions';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const ChangeChart = ({
    handleChange,
    chartType,
    getChartType
}) => {
    const classes = useStyles()
    const inputLabel = useRef(null)
    const [labelWidth, setLabelWidth] = useState(0)

    useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth)
        getChartType()
    }, []);

    return (
        <div>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel ref={inputLabel} htmlFor="outlined-chart-type">
                    Chart Type
                </InputLabel>
                <Select
                    value={chartType}
                    onChange={handleChange}
                    labelWidth={labelWidth}
                    inputProps={{
                        name: 'type',
                        id: 'outlined-chart-type',
                    }}
                >
                    <MenuItem value="ScatterChart">Scatter Chart</MenuItem>
                    <MenuItem value="BarChart">BarChart</MenuItem>
                    <MenuItem value="ColumnChart">ColumnChart</MenuItem>
                </Select>
            </FormControl>
        </div>
    );
}

const mapToState = state => {
    return {
        chartType: state.chart.chartType
    }
}

const mapToDispatch = dispatch => {
    return {
        handleChange: chartType => { dispatch(chartActions.changeChartType(chartType.target.value)) },
        getChartType: () => dispatch(chartActions.getChartType())
    }
}
export default connect(mapToState, mapToDispatch)(ChangeChart);