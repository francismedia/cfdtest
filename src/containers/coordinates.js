import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import MaterialTable from 'material-table';
import coordinatesActions from '../actions/coordinates.actions';

const Coordinates = ({
    fetch,
    add,
    update,
    remove,
    coordinates,
    hasUpdate
}) => {
    const columns = [
        { title: 'X', field: 'x' },
        { title: 'Y', field: 'y' },
    ];
    useEffect(() => {
        fetch();
    }, [hasUpdate]);

    return (<MaterialTable
        title="Coordinates"
        columns={columns}
        data={coordinates}
        editable={{
            onRowAdd: newData => new Promise((resolve) => { add(newData); resolve() }),
            onRowUpdate: (newData, oldData) => new Promise((resolve) => { update(newData, oldData); resolve() }),
            onRowDelete: oldData => new Promise((resolve) => { remove(oldData); resolve(); }),
        }}
    />)
}

const mapState = state => {
    return {
        coordinates: state.main.coordinates,
        hasUpdate: state.main.hasUpdate
    }
}

const mapDispatch = dispatch => {
    return {
        fetch: () => dispatch(coordinatesActions.getCoordinates()),
        add: data => { dispatch(coordinatesActions.addCoordinates(data)) },
        update: (newData, oldData) => { dispatch(coordinatesActions.updateCoordinates(newData, oldData)) },
        remove: data => { dispatch(coordinatesActions.removeCoordinates(data)) }
    }
}
export default connect(mapState, mapDispatch)(Coordinates);