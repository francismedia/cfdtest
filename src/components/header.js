import React from 'react'
import {
    Link
} from "react-router-dom";

const Header = () => (
    <header>
        <div className="container">
            <div className="logo">CFD Test</div>
            <div className="nav">
                <ul>
                    <li>
                        <Link to="/coordinates">Coordinates</Link>
                    </li>
                    <li>
                        <Link to="/chart">Chart</Link>
                    </li>
                </ul>
            </div>
        </div>
    </header>
);

export default Header;
