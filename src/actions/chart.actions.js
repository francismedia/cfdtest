import { CHANGE_CHART_TYPE, GET_CHART_TYPE } from "../constants/chart.constants"

export const getChartType = () => { return { type: GET_CHART_TYPE } }
export const changeChartType = (chartType) => { return { type: CHANGE_CHART_TYPE, chartType } }

const chartActions = {
    changeChartType,
    getChartType
}
export default chartActions