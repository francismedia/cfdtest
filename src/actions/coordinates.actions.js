import { GET_ITEMS, ADD_ITEM, UPDATE_ITEM, REMOVE_ITEM } from '../constants/coordinate.constants'

export const getCoordinates = () => { return { type: GET_ITEMS } }
export const addCoordinates = data => { return { type: ADD_ITEM, data } }
export const updateCoordinates = (newData, oldData) => { return { type: UPDATE_ITEM, data: { newData, oldData } } }
export const removeCoordinates = data => { return { type: REMOVE_ITEM, data } }
const coordinatesActions = {
    getCoordinates,
    addCoordinates,
    updateCoordinates,
    removeCoordinates
}
export default coordinatesActions;